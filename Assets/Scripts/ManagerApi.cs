using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class ManagerApi : MonoBehaviour {
	private const string API_KEY     = "88b6e2032ede8bd56d491af15a9369b1";
	private const string BASE_URL    = "http://api.openweathermap.org/data";
	private const string VERSION_API = "2.5";

	private string resultURL = "";

	[SerializeField] private string cityId;
	[SerializeField] private Text   cityText;
	[SerializeField] private Text   tempMinText;
	[SerializeField] private Text   tempMaxText;
	[SerializeField] private Text   descriptionText;

	private void Start() {
		resultURL = $"{BASE_URL}/{VERSION_API}/weather?q={cityId}&appid={API_KEY}&units=metric&lang=RU";
		WeatherInfo weatherInfo = GetWeather();
		print(weatherInfo.name);
		weatherInfo.weather.ForEach(x => print(x.description));

		cityText.text        = $"{weatherInfo.name}";
		tempMinText.text     = $"Минимальная температура: {weatherInfo.main.temp_min}";
		tempMaxText.text     = $"Максимальная температура: {weatherInfo.main.temp_max}";
		descriptionText.text = $"{weatherInfo.weather[0].description}";

		print($"Минимальная температура: {weatherInfo.main.temp_min}");
		print($"Максимальная температура: {weatherInfo.main.temp_max}");
		print($"Ощущается как: {weatherInfo.main.feels_like}");
	}

	private WeatherInfo GetWeather() {
		HttpWebRequest  request  = (HttpWebRequest) WebRequest.Create(resultURL);
		HttpWebResponse response = (HttpWebResponse) request.GetResponse();

		StreamReader reader       = new StreamReader(response.GetResponseStream());
		string       jsonResponse = reader.ReadToEnd();

		return JsonUtility.FromJson<WeatherInfo>(jsonResponse);
	}
}
