using System.IO;
using System.Net;
using Nasa;
using UnityEngine;
using UnityEngine.UI;

public class NasaController : MonoBehaviour {
    // Account Email: vinty1978@gmail.com
    // Account ID: 013eb5aa-45fc-4a73-95ca-1f75498e7a5c
    [SerializeField] private Text asteroidCount;


    private const string BASE_URL  = "https://api.nasa.gov/neo/rest/v1/feed";
    private const string PARAM_KEY = "?api_key=";
    private const string KEY_NASA  = "VXombzwiOk4kUsCKiIpubNuBlt0LrNLSR41bL7AB";
    private       string resultUrl = $"{BASE_URL}{PARAM_KEY}{KEY_NASA}";

    private void Start() {
        NasaInfo nasaInfo = GetNasaInfo();
        asteroidCount.text = $"Всего астероидов вокруг земли: {nasaInfo.element_count}";
        print(nasaInfo.element_count);
        print(nasaInfo.nearEarthObjects.data);
    }

    private NasaInfo GetNasaInfo() {
        HttpWebRequest  request  = (HttpWebRequest) WebRequest.Create(resultUrl);
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();

        StreamReader reader       = new StreamReader(response.GetResponseStream());
        string       jsonResponse = reader.ReadToEnd();

        return JsonUtility.FromJson<NasaInfo>(jsonResponse);
    }
}
