using System;
using System.Collections.Generic;

namespace Nasa {
    [Serializable]
    public class NasaInfo {
        public string       element_count;
        public NearEarthObject nearEarthObjects;
    }
}
